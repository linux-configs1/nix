{ config, pkgs, ... }:

{
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  home.username = "hamza";
  home.homeDirectory = "/home/hamza";

  home.packages = with pkgs; [
    neofetch
    ripgrep
    htop

    # Emacs
    mononoki

    # Needed for XMonad config
    picom

	st

    alacritty
    haskellPackages.xmobar
    rofi
    hsetroot
    xscreensaver

  ];

  programs = {

    git = {
      enable = true;
      userName = "hamzashahid.blit";
      userEmail = "hamzashahid@tutanota.com";
    };

    emacs = {
      enable = true;
    };

    bat.enable = true;

  };

  #home.file.".emacs.d".source = ./.emacs.d;
  #home.file.".xmonad".source = ./.xmonad;

  home.stateVersion = "21.05";
}
